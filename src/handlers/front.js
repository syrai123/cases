
const Boom = require('boom');

const debug = require('debug')('api:handlers:user');

debug('Loading auth handler.');

const prepareDataAndReplyCase = (reply, { caseModel, caseInfo, media, points, tags }, helper) => {

  let tagsText = '';
  tags.forEach((tag) => {

    tagsText += `#${tag.name} `;
  });

  points.unshift({ name: 'Автор (-и)', description: caseInfo[0].authors, position: 5 });
  points.unshift({ name: 'Дата', description: caseInfo[0].date, position: 4 });
  points.unshift({ name: 'Країна (-и)', description: caseInfo[0].countries, position: 3 });

  helper.sorter.sort(points, 'position');

  points.forEach((e) => {

    e.description = helper.parser.parseToArrBy(e.description, '\n');
  });

 // points.forEach((e) => {

 //   e.description = e.description.map(elem => helper.selector.select(elem, ':', 50));
 // });

  let x = '';
  points.forEach((e) => {

    x += e.position;
  });
  console.log(x);

  reply.view('current_project', {
    image: caseModel.small_image,
    name: caseInfo[0].name,
    date: caseInfo[0].date,
    countries: caseInfo[0].countries,
    tags: tagsText,
    points
  });
};

function create({ config, models, helper, _libs }) {

  return {
    getCss(request, reply) {

      reply.file(`./src/css/${request.params.cssName}`, { confine: false });

    },

    getScript(request, reply) {

      reply.file(`./src/frontScripts/${request.params.scriptName}`, { confine: false });

    },

    getFile(request, reply) {

      reply.file(`./public/${request.params.fileName}`, { confine: false });
    },

    getPage(request, reply) {

      reply.file(`./src/templates/${request.params.htmlName}.html`, { confine: false });
    },

    getCase(request, reply) {

      const { id } = request.params;
      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      cm.get(id, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {

          reply(Boom.badImplementation());

          return;
        }
        prepareDataAndReplyCase(reply, res, helper);

      });


    },
    getMainPage(request, reply) {

      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      const filters = null;
      cm.list({ count: 3, position: 0, sort: 'DESC' }, filters, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {

          reply(Boom.badImplementation());

          return;
        }
        helper.textLength.correctArrByParam(res, 'short_description', config.front.mainPage.caseDescTextLength);
        reply.view('index', { cases: res });

      });

    },
    getAdminka(_request, reply) {

      reply.file('./src/templates/secure/adminPanel.html', { confine: false });

    }
  };


}

module.exports = { create };

