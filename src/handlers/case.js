
const Boom = require('boom');

const debug = require('debug')('api:handlers:user');

debug('Loading auth handler.');

function create({ config, models, helper, _libs }) {

  return {
    getCases(request, reply) {

      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      const { tags, count, position, sort, search } = request.query;

      const cb = (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply(res);
      };

      cm.list({ count, position, sort, search }, tags, cb);

    },
    getCount(request, reply) {

      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      const { tags } = request.query;
      cm.count(tags, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply(res);
      });

    },
    getCase(request, reply) {

      const { id } = request.params;
      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      cm.get(id, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {

          reply(Boom.badImplementation());

          return;
        }
        reply(res);

      });
    },
    createCase(request, reply) {

      const { credentials } = request.auth;

     /* if (!credentials.isAdmin()) {
        reply(Boom.unauthorized('insufficient rights'));

        return;
      } */


      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      const { caseModel, caseInfo, tags, media, points } = request.payload;
      cm.insert({ caseModel, caseInfo, tags, media, points }, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } if (err) {

          reply(Boom.badImplementation());

          return;
        }
        reply(res);

      });
    },
    updateCase(request, reply) {

      const { id } = request.params;
      const { caseModel, caseInfoUpdate, caseInfoAdd, mediaUpdate, mediaAdd,
          pointsUpdate, pointsAdd } = request.payload;
      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      cm.update({ id,
        caseModel,
        caseInfoUpdate,
        caseInfoAdd,
        mediaUpdate,
        mediaAdd,
        pointsUpdate,
        pointsAdd },
          (err) => {

            if (err && err.isBoom) {

              reply(err);

              return;
            } else if (err) {

              reply(Boom.badImplementation());

              return;
            }
            reply().code(204);

          });

    },
    deleteCase(request, reply) {

      const { credentials } = request.auth;

      /* if (!credentials.isAdmin()) {
        reply(Boom.unauthorized('insufficient rights'));

        return;
      } */

      const { id } = request.params;
      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      cm.delete(id, (err) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {

          reply(Boom.badImplementation());

          return;
        }
        reply().code(204);

      });

    },
    addTags(request, reply) {

      const { credentials } = request.auth;

      /* if (!credentials.isAdmin()) {
         reply(Boom.unauthorized('insufficient rights'));

         return;
       } */

      const { id } = request.params;
      const { tags } = request.query;

      if (!tags || !Array.isArray(tags) || tags.length === 0) {

        reply().code(204);

        return;
      }
      const cm = models.CaseModel.create( { db: request.app.db, config, helper });
      cm.addTags(id, tags, (err) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {

          reply(Boom.badImplementation());

          return;
        }
        reply().code(204);

      });

    }
  };
}

module.exports = { create };

