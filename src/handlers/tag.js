
const Boom = require('boom');

const debug = require('debug')('api:handlers:user');

debug('Loading auth handler.');

function create({ config, models, helper, _libs }) {

  return {
    getTags(request, reply) {

      const tm = models.TagModel.create( { db: request.app.db, config, helper });
      tm.list((err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply(res);
      });
    },
    createTag(request, reply) {

      const { credentials } = request.auth;

      /* if (!credentials.isAdmin()) {
        reply(Boom.unauthorized('insufficient rights'));

        return;
      }*/

      const tm = models.TagModel.create( { db: request.app.db, config, helper });
      const { name, id_category } = request.payload;
      tm.insert({ name, id_category }, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply(res);
      });
    },
    createCategory(request, reply) {

      const { credentials } = request.auth;

      /* if (!credentials.isAdmin()) {
         reply(Boom.unauthorized('insufficient rights'));

         return;
       }*/

      const tm = models.TagModel.create( { db: request.app.db, config, helper });
      const { name } = request.payload;
      tm.insertCategory(name, (err, res) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply(res);
      });
    },
    deleteTag(request, reply) {

      const { credentials } = request.auth;

      /* if (!credentials.isAdmin()) {
         reply(Boom.unauthorized('insufficient rights'));

         return;
       }*/

      const tm = models.TagModel.create( { db: request.app.db, config, helper });
      const { id } = request.params;
      tm.delete(id, (err) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply().code(204);
      });
    },
    deleteCategory(request, reply) {

      const { credentials } = request.auth;

      /* if (!credentials.isAdmin()) {
        reply(Boom.unauthorized('insufficient rights'));

        return;
      }*/

      const tm = models.TagModel.create( { db: request.app.db, config, helper });
      const { id } = request.params;
      tm.deleteCategory(id, (err) => {

        if (err && err.isBoom) {

          reply(err);

          return;
        } else if (err) {
          reply(Boom.badImplementation());
          console.log(err);

          return;
        }
        reply().code(204);
      });
    }
  };
}

module.exports = { create };

