const Boom = require('boom');
const debug = require('debug')('api:handlers:auth');

debug('Loading auth handler.');

function create({ config, models, helper, libs }) {

  return {
    postAuth(request, reply) {

      const um = models.UserModel.create( { db: request.app.db, config, helper });

      um.verify(request.payload.login, request.payload.password, (err, result) => {

        if (err) {
          console.error(err)
          reply(Boom.badImplementation('UserModel error.'));

          return;
        }

        if (!result.success) {
          reply(Boom.unauthorized('Wrong username or password.'));

          return;
        }

        const session = {
          valid: true,
          id: result.id,
          hash: result.hash,
          iat: new Date().getTime(),
          exp: new Date().getTime() + config.auth.lifeTime,
          scope: result.scope,
          role: result.role
        };

        // sign the session as a JWT
        const token = libs.JWT.sign(session, config.auth.jwtSecret ); // synchronous

        reply({ id: result.id })
          .header('Authorization', `Token ${token}`)
          .state('token', token, config.auth.cookie)
          .code(200);
      });

    }

  };
}

module.exports = { create };
