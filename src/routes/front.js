const Joi = require('joi');

const internals = {};

internals.applyRoutes = function applyRoutes(server, next) {

  server.route({
    path: '/internals/css/{cssName}',
    method: 'GET',
    handler: internals.handlers.getCss,
    config: {
      description: 'Get css by name',
      tags: ['internals'],
      notes: ['Get css by name'],
      validate: {
        params: {
          cssName: Joi.string().required()
        }
      },
      auth: false
    }
  });

  server.route({
    path: '/public/{fileName}',
    method: 'GET',
    handler: internals.handlers.getFile,
    config: {
      description: 'Get file by name',
      tags: ['public'],
      notes: ['Get file by name'],
      validate: {
        params: {
          fileName: Joi.string().required()
        }
      },
      auth: false
    }
  });

  server.route({
    path: '/internals/scripts/{scriptName}',
    method: 'GET',
    handler: internals.handlers.getScript,
    config: {
      tags: ['internals'],
      validate: {
        params: {
          scriptName: Joi.string().required()
        }
      },
      auth: false
    }
  });

  server.route({
    path: '/',
    method: 'GET',
    handler: internals.handlers.getMainPage,
    config: {
      tags: ['template'],
      auth: false
    }
  });

  server.route({
    path: '/{htmlName}',
    method: 'GET',
    handler: internals.handlers.getPage,
    config: {
      validate: {
        params: {
          htmlName: Joi.string().required()
        }

      },
      tags: ['template'],
      auth: false
    }
  });

  server.route({
    path: '/cases/{id}',
    method: 'GET',
    handler: internals.handlers.getCase,
    config: {
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        }

      },
      tags: ['template'],
      auth: false
    }
  });

  server.route({
    path: '/secure/adminka',
    method: 'GET',
    handler: internals.handlers.getAdminka,
    config: {
      tags: ['template'],
      auth: false
    }
  });

  next();
};

module.exports.register = function register(server, options, next) {

  internals.handlers = options.handlers;

  server.dependency(['hapi-plugin-mysql'], internals.applyRoutes);
  next();
};

module.exports.register.attributes = {
  name: 'user'
};
