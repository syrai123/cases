const Joi = require('joi');

const internals = {};

internals.applyRoutes = function applyRoutes(server, next) {

  server.route({
    path: '/tags',
    method: 'GET',
    handler: internals.handlers.getTags,
    config: {
      description: 'Get tags in tag categories',
      tags: ['api'],
      notes: ['Get tags'],
      auth: false
    }
  });

  server.route({
    path: '/tags',
    method: 'POST',
    handler: internals.handlers.createTag,
    config: {
      description: 'create a tag',
      tags: ['api'],
      notes: ['create a tag'],
      validate: {
        payload: {
          id_category: Joi.number().integer().positive().required(),
          name: Joi.string().required()
        }
      },
   //   auth: false
    }
  });

  server.route({
    path: '/tags/categories',
    method: 'POST',
    handler: internals.handlers.createCategory,
    config: {
      description: 'create a tag category',
      tags: ['api'],
      notes: ['create a tag category'],
      validate: {
        payload: {
          name: Joi.string().required()
        }
      },
     // auth: false
    }
  });


  server.route({
    path: '/tags/{id}',
    method: 'DELETE',
    handler: internals.handlers.deleteTag,
    config: {
      description: 'delete tag',
      tags: ['api'],
      notes: ['delete tag'],
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        }
      }
    }
  });

  server.route({
    path: '/tags/categories/{id}',
    method: 'DELETE',
    handler: internals.handlers.deleteCategory,
    config: {
      description: 'delete tag',
      tags: ['api'],
      notes: ['delete tag'],
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        }
      }
    }
  });

  next();
};

module.exports.register = function register(server, options, next) {

  internals.handlers = options.handlers;

  server.dependency(['hapi-plugin-mysql'], internals.applyRoutes);
  next();
};

module.exports.register.attributes = {
  name: 'tag'
};
