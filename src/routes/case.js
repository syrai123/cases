/* eslint-disable newline-per-chained-call */
const Joi = require('joi');

const internals = {};

internals.applyRoutes = function applyRoutes(server, next) {

  server.route({
    path: '/cases',
    method: 'GET',
    handler: internals.handlers.getCases,
    config: {
      validate: {
        query: {
          count: Joi.number().integer().min(1).max(1000).default(100),
          position: Joi.number().integer().min(0).max(1000).default(0),
          sort: Joi.string().allow('ASC', 'DESC'),
          tags: Joi.array().items(Joi.number().integer().positive()),
          search: Joi.string()
        }
      },
      description: 'Get cases',
      tags: ['api'],
      notes: ['Get cases'],
      auth: false
    }
  });

  server.route({
    path: '/cases/count',
    method: 'GET',
    handler: internals.handlers.getCount,
    config: {
      validate: {
        query: {
          tags: Joi.array().items(Joi.number().integer().positive())
        }
      },
      description: 'Get cases',
      tags: ['api'],
      notes: ['Get cases'],
      auth: false
    }
  });

  server.route({
    path: '/cases/{id}',
    method: 'GET',
    handler: internals.handlers.getCase,
    config: {
      description: 'Get case by id',
      tags: ['api'],
      notes: ['Get case by id'],
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        }
      },
      auth: false
    }
  });

  server.route({
    path: '/cases',
    method: 'POST',
    handler: internals.handlers.createCase,
    config: {
      validate: {
        payload: {
          caseModel: Joi.object().keys({
            small_image: Joi.string().max(254).required()
          }).required(),
          caseInfo: Joi.array().items(Joi.object().keys({
            language: Joi.string().allow('en', 'ua', 'all').default('all'),
            name: Joi.string().max(254),
            short_description: Joi.string(),
            authors: Joi.string().max(1023),
            countries: Joi.string().max(255),
            date: Joi.string().max(255)
          })).required(),
          tags: Joi.array().items(Joi.object().keys({
            id_tag: Joi.number().integer().positive()
          })),
          media: Joi.array().items(Joi.object().keys({
            reference: Joi.string().max(1023).required(),
            type: Joi.string().allow('image').default('image').required()
          })),
          points: Joi.array().items(Joi.object().keys({
            position: Joi.number().integer().positive().required(),
            type: Joi.string().allow('point', 'category').default('point'),
            language: Joi.string().allow('en', 'ua', 'all').default('all'),
            name: Joi.string().max(254),
            description: Joi.string()
          }))
        }
      },
      description: 'create case',
      tags: ['api'],
      notes: ['create case'],
      //  auth: false
    }
  });

  server.route({
    path: '/cases/{id}',
    method: 'PUT',
    handler: internals.handlers.updateCase,
    config: {
      description: 'update case',
      tags: ['api'],
      notes: ['update case'],
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        },
        payload: {
          caseModel: Joi.object().keys({
            small_image: Joi.string().max(254)
          }),
          caseInfoUpdate: Joi.array().items(Joi.object().keys({
            id: Joi.number().integer().positive().required(),
            language: Joi.string().allow('en', 'ua', 'all'),
            name: Joi.string().max(254),
            short_description: Joi.string(),
            authors: Joi.string().max(1023),
            countries: Joi.string().max(255),
            date: Joi.string().max(255)
          })),
          caseInfoAdd: Joi.array().items(Joi.object().keys({
            language: Joi.string().allow('en', 'ua', 'all').required(),
            name: Joi.string().max(254),
            short_description: Joi.string().max(1023),
            authors: Joi.string().max(1023),
            countries: Joi.string().max(255)
          })),
          mediaUpdate: Joi.array().items(Joi.object().keys({
            id: Joi.number().integer().positive().required(),
            reference: Joi.string().max(255),
            type: Joi.string().allow('image').default('image')
          })),
          mediaAdd: Joi.array().items(Joi.object().keys({
            reference: Joi.string().max(255),
            type: Joi.string().allow('image').default('image')
          })),
          pointsUpdate: Joi.array().items(Joi.object().keys({

            id: Joi.number().integer().positive().required(),
            position: Joi.number().integer().positive(),
            type: Joi.string().allow('point', 'category').default('point'),
            language: Joi.string().allow('en', 'ua', 'all').default('all'),
            name: Joi.string().max(254),
            description: Joi.string()
          })),
          pointsAdd: Joi.array().items(Joi.object().keys({

            position: Joi.number().integer().positive(),
            type: Joi.string().allow('point', 'category').default('point'),
            language: Joi.string().allow('en', 'ua', 'all').default('all'),
            name: Joi.string().max(254),
            description: Joi.string()
          }))

        }
      }
    }
  });

  server.route({
    path: '/cases/{id}',
    method: 'DELETE',
    handler: internals.handlers.deleteCase,
    config: {
      description: 'delete case',
      tags: ['api'],
      notes: ['delete case'],
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        }
      }
    }
  });

  server.route({
    path: '/cases/{id}/tags',
    method: 'PUT',
    handler: internals.handlers.addTags,
    config: {
      description: 'add tags to the case',
      tags: ['api'],
      notes: ['add tags to the case'],
      validate: {
        params: {
          id: Joi.number().integer().positive().required()
        },
        query: {
          tags: Joi.array().items(Joi.number().integer().positive())
        }
      },
    //  auth: false
    }
  });

  next();
};

module.exports.register = function register(server, options, next) {

  internals.handlers = options.handlers;

  server.dependency(['hapi-plugin-mysql'], internals.applyRoutes);
  next();
};

module.exports.register.attributes = {
  name: 'case'
};
