const Joi = require('joi');

const internals = {};

internals.applyRoutes = function applyRoutes(server, next) {

  server.route({
    path: '/login',
    method: 'POST',
    handler: internals.handlers.postAuth,
    config: {
      description: 'Login with username/password',
      tags: ['api'],
      notes: ['Login with username/password to get JWT token.'],
      validate: {
        payload: {
          login: Joi.string().example('admin').required(),
          password: Joi.string().example('qwerty').required()
        }
      },
      auth: false
    }
  });


  next();
};

module.exports.register = function register(server, options, next) {

  internals.handlers = options.handlers;

  server.dependency(['hapi-plugin-mysql'], internals.applyRoutes);
  next();
};

module.exports.register.attributes = {
  name: 'auth'
};
