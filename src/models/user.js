

const debug = require('debug')('api:models:users');
const Boom = require('boom');

debug('Loading user model.');

function toClient(result) {

  if (!result) {
    return result;
  }

  if (Array.isArray(result)) {
    return result.map( item => toClient(item));
  }


  if (result.password) {
    const cleanResult = Object.assign({}, result);
    delete cleanResult.password;

    return cleanResult;
  }

  return result;
}

class User {

  constructor({ db, config, helper }) {

    this.db = db;
    this.config = config;
    this.table = 'users';
    this.helper = helper;
  }

  list(cb) {

    this.db.query('SELECT * FROM users WHERE deleted_at IS NULL', (err, result) => cb(err, toClient(result)) );
  }

  getProfile(id, cb) {

    this.db.query('SELECT * FROM users WHERE id = ? AND deleted_at IS NULL', [id], (queryerr, users) => {

      if (queryerr) {
        cb(Boom.badImplementation('db error'), null);

        return;
      }

      if (!users || users.length === 0) {
        cb(Boom.notFound('user not found'), null);

        return;
      }

      const user = toClient(users[0]);
      cb(null, user);

    });
  }

  isMailUnique(mail, cb) {

    this.db.query('SELECT * FROM users WHERE email = ? AND deleted_at IS NULL', [mail], (err, result) => {

      if (err) {
        cb(Boom.internal('Internal DB error'));

        return;
      }
      cb(null, !result || result.length === 0);
    });
  }

  verify(login, password, cb) {

    this.db.query('SELECT * FROM users WHERE login = ? AND deleted_at IS NULL', [login], (queryerr, users) => {

      if (queryerr) {
        cb(queryerr);

        return;
      }

      if (!users || users.length === 0) {
        const result = { success: false };
        cb(undefined, result);

        return;
      }

      const user = users[0];

      const { comparePassword } = this.helper.password;

      comparePassword(password, user.password, (err, res) => {

        if (err) {
          cb(new Error('Error comparing passwords.'));

          return;
        }

        if (res === false) {
          const result = { success: false };
          cb(undefined, result);

          return;
        }

        const result = {
          success: true,
          id: user.id,
          scope: 'user',
          role: [user.role],
          username: user.email,
          hash: user.password
        };

        cb(undefined, result);
      });
    });
  }

  getHash(id, cb) {

    const query = 'SELECT password FROM users WHERE id = ? AND deleted_at IS NULL';
    this.db.query(query, id, (err, users) => {

      if (err) {
        cb(Boom.internal('Internal DB error'));

        return;
      }

      const user = users[0];
      cb(null, user);
    });

  }

  insert(user, cb) {

    const { convert } = this.helper.converter;
    const userDb = convert(user, [{ replace: 'postalCode', with: 'postal_code' }]);

    const query = 'INSERT INTO users SET ?';
    this.db.query(query, userDb, (err, result) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb(null, result);
    });
  }

  update(user, cb) {

    const query = 'UPDATE users SET ? WHERE id = ?';
    this.db.query(query, [user, user.id], (err, result) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb(null, result);
    });

  }

  usersList(cb) {

    const query = 'SELECT id, email, first_name as firstName, last_name as lastName, avatar FROM users WHERE deleted_at IS NULL';
    this.db.query(query, (queryerr, users) => {

      if (queryerr) {
        cb(Boom.badImplementation('db error'), null);

        return;
      }

      cb(null, users);

    });
  }

}

const create = function create(params) {

  return new User(params);
};

module.exports = {
  create
};
