/* eslint-disable consistent-return,hapi/hapi-scope-start,no-plusplus,no-param-reassign,newline-per-chained-call,max-len */

const debug = require('debug')('api:models:cases');
const Boom = require('boom');

debug('Loading case model.');

const wheres = (caseIds) => {

  let result = '';

  caseIds.forEach((_e) => {

    if (result === '') {

      result += ' WHERE cases.id = ?';
    } else {

      result += ' OR cases.id = ? ';
    }
  });

  return result;
};

const parseTagsInQuestions = (tags) => {

  let questions = '';
  for (let i = 0; i < tags.length; i++) { questions += ', ? '; }
// 1я запятая нам не нужна

  return questions.substr(1);
};

const getCaseIdsByTags = (db, tags) => new Promise((resolve, reject) => {

  if (!tags && !Array.isArray(tags)) {

    return resolve(null);
  }

  const query = 'SELECT cases.*, COUNT(*) AS c ' +
          'FROM cases_tags,  cases ' +
          'WHERE cases_tags.id_case = cases.id ' +
          `AND cases_tags.id_tag IN (${parseTagsInQuestions(tags)}) ` +
          'GROUP BY cases.id ' +
          'HAVING c = ?';

  const params = tags.concat([tags.length]);


  db.query(query, params, (err, res) => {

    if (err) {

      console.log(err);

      return reject(err);
    }

    const casesIds = [];

    res.forEach((e) => {

      casesIds.push(e.id);
    });

    return resolve( casesIds);
  });
} );

const getCaseIdsBySearch = (db, search) => new Promise((resolve, reject) => {

  db.query('SELECT * FROM cases_info WHERE name like ?', [`%${search}%`], (err, result) => {

    if (err) {

      return reject(err);
    }
    const casesIds = [];

    result.forEach((e) => {

      casesIds.push(e.id_case);
    });

    return resolve( casesIds);

  });
});

class Case {

  constructor({ db, config, helper }) {

    this.db = db;
    this.config = config;
    this.helper = helper;
    debug('constructor was created');
  }

  list({ count = 1000, position = 0, sort = 'ASC', search }, filters, cb) {

    Promise.resolve()
      .then(() => {

        if (search) {

          return getCaseIdsBySearch(this.db, search);
        }

        if (!filters || !Array.isArray(filters) || filters.length === 0) {

          return;
        }

        return getCaseIdsByTags(this.db, filters);

      }).then(caseIds => new Promise((resolve, reject) => {

        console.log(caseIds);
        const query = 'SELECT * FROM cases LEFT JOIN cases_info ON cases.id=cases_info.id_case ';
        const sortAndLimit = ` ORDER BY cases.id ${sort} LIMIT ?, ?;`;
        let fullQuery;
        const params = [];
        if (!caseIds) {

          fullQuery = query + sortAndLimit;
        } else if (caseIds && caseIds.length === 0) {

          return reject(Boom.notFound('Not found anyone case with such tags!'));
        } else {

          fullQuery = query + wheres(caseIds) + sortAndLimit;

          caseIds.forEach(e => params.push(e));

        }
        params.push(position);
        params.push(count);

        this.db.query(fullQuery, params, (err, result) => {

          if (err) {

            console.error(err);

            return reject(err);

          }

          if (result.length === 0) {

            return reject(Boom.notFound('Not found anyone case with such tags!'));
          }

          resolve( { result, caseIds } );
        });
      })).then(({ result, caseIds }) => {

        let query = '';
        const params = [];

        // когда фильтрация или поиск есть
        if (caseIds) {

          caseIds.forEach((e) => {

            params.push(e);
            query += 'SELECT * FROM cases_tags LEFT JOIN tags ON (cases_tags.id_tag = tags.id) WHERE id_case = ?;';
          });
        } else { // когда нет

          result.forEach((e) => {

            params.push(e.id);
            query += 'SELECT * FROM cases_tags LEFT JOIN tags ON (cases_tags.id_tag = tags.id) WHERE id_case = ?;';
          });
        }

        this.db.query(query, params, (err, res) => {

          if (err) {

            return cb(Boom.internal('Internal DB error'));
          }

          // Вот что бывает когда проект надо сдать за 6 дней. Извини меня, друг!
          result.forEach((e) => {

            if (Array.isArray(res[0])) {
              res.forEach((tagArray) => {

                if ((tagArray && Array.isArray(tagArray) && tagArray.length !== 0 && e.id === tagArray[0].id_case)) {

                  e.tags = tagArray;
                }
              });
            } else {
              e.tags = [];
              res.forEach((tag) => {

                if (tag.id_case === e.id) e.tags.push(tag);
              });
            }
          });

          return cb(null, result);
        });

      }).catch((err) => {

        // console.error(err);
        cb(err);
      });

  }

  count(filters, cb) {

    getCaseIdsByTags(this.db, filters)
      .then((ids) => {
        if (ids && Array.isArray(ids)) {

          return cb(null, ids.length);
        }

        this.db('SELECT COUNT(*) AS c FROM cases', (err, res) => {

          if (err) throw err;
          cb(null, res.c);
        });
      })
      .catch(err => cb(err));
  }

  insert({ caseModel, caseInfo, tags, media, points }, cb) {

    // TODO можно выделить каждыый блок в одну функцию с
      // параметрами "arr" "table name" "resolve" "reject". Ну или нет....)

    Promise.resolve().then(() => new Promise( (resolve, reject ) => {

      this.db.query('INSERT INTO cases SET ?', caseModel, (err, result) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve(result.insertId);
      });
    })).then(caseId => new Promise( (resolve, reject ) => {

      if (!caseInfo || !Array.isArray(caseInfo) || caseInfo.length === 0) {

        return resolve(caseId);
      }
      let query = '';
      for ( let i = 0; i < caseInfo.length; i++) {

        caseInfo[i].id_case = caseId;
        query += 'INSERT INTO cases_info SET ?;';
      }
      this.db.query(query, caseInfo, (err) => {

        if (err) {

          console.error(err);

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve(caseId);
      });
    })).then(caseId => new Promise( (resolve, reject ) => {

      if (!tags || !Array.isArray(tags) || tags.length === 0) {

        return resolve(caseId);
      }
      let query = '';
      for ( let i = 0; i < tags.length; i++) {

        tags[i].id_case = caseId;
        query += 'INSERT INTO cases_tags SET ?;';
      }
      this.db.query(query, tags, (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve(caseId);
      });
    })).then(caseId => new Promise( (resolve, reject ) => {

      if (!media || !Array.isArray(media) || media.length === 0) {

        return resolve(caseId);
      }
      let query = '';
      for ( let i = 0; i < media.length; i++) {

        media[i].id_case = caseId;
        query += 'INSERT INTO media SET ?;';
      }
      this.db.query(query, media, (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve(caseId);
      });
    })).then(caseId => new Promise( (resolve, reject ) => {

      if (!points || !Array.isArray(points) || points.length === 0) {

        return resolve(caseId);
      }

      let query = '';
      for ( let i = 0; i < points.length; i++) {

        points[i].id_case = caseId;
        query += 'INSERT INTO points SET ?;';
      }

      this.db.query(query, points, (err) => {

        if (err) {

          console.error(err);

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve(caseId);
      });


    })).then((res) => {

      cb(null, res);

    }).catch((e) => {

      console.error(e);
      cb(e);

    });

  }

  update({ id, caseModel, caseInfoUpdate, caseInfoAdd, mediaUpdate, mediaAdd, pointsUpdate, pointsAdd }, cb) {

    Promise.resolve().then(() => new Promise( (resolve, reject ) => {

      if (!caseModel) {

        return resolve();
      }

      this.db.query('UPDATE cases SET ? WHERE id = ?', [caseModel, id], (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then(() => new Promise( (resolve, reject ) => {

      if (!caseInfoUpdate || !Array.isArray(caseInfoUpdate) || caseInfoUpdate.length === 0) {

        return resolve();
      }
      let query = '';
      const params = [];
      for ( let i = 0; i < caseInfoUpdate.length; i++) {

        params.push(caseInfoUpdate[i]);
        params.push(caseInfoUpdate[i].id);
        query += 'UPDATE cases_info SET ? WHERE id = ?;';
      }
      this.db.query(query, params, (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then(() => new Promise( (resolve, reject ) => {

      if (!caseInfoAdd || !Array.isArray(caseInfoAdd) || caseInfoAdd.length === 0) {

        return resolve();
      }
      let query = '';
      for ( let i = 0; i < caseInfoAdd.length; i++) {

        caseInfoAdd[i].id_case = id;
        query += 'INSERT INTO cases_info SET ?;';
      }
      this.db.query(query, caseInfoAdd, (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then(() => new Promise( (resolve, reject ) => {

      if (!mediaUpdate || !Array.isArray(mediaUpdate) || mediaUpdate.length === 0) {

        return resolve();
      }
      let query = '';
      const params = [];
      for ( let i = 0; i < mediaUpdate.length; i++) {

        params.push(mediaUpdate[i]);
        params.push(mediaUpdate[i].id);
        query += 'UPDATE media SET ? WHERE id = ?;';
      }
      this.db.query(query, params, (err) => {

        if (err) {

          console.error(err);

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then(() => new Promise( (resolve, reject ) => {

      if (!mediaAdd || !Array.isArray(mediaAdd) || mediaAdd.length === 0) {

        return resolve();
      }
      let query = '';
      for ( let i = 0; i < mediaAdd.length; i++) {

        mediaAdd[i].id_case = id;
        query += 'INSERT INTO media SET ?;';
      }
      this.db.query(query, mediaAdd, (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then(() => new Promise( (resolve, reject ) => {

      if (!pointsUpdate || !Array.isArray(pointsUpdate) || pointsUpdate.length === 0) {

        return resolve();
      }
      let query = '';
      const params = [];
      for ( let i = 0; i < pointsUpdate.length; i++) {

        params.push(pointsUpdate[i]);
        params.push(pointsUpdate[i].id);
        query += 'UPDATE points SET ? WHERE id = ?;';
      }
      this.db.query(query, params, (err) => {

        if (err) {

          console.error(err);

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then(() => new Promise( (resolve, reject ) => {

      if (!pointsAdd || !Array.isArray(pointsAdd) || pointsAdd.length === 0) {

        return resolve();
      }
      let query = '';
      for ( let i = 0; i < pointsAdd.length; i++) {

        pointsAdd[i].id_case = id;
        query += 'INSERT INTO points SET ?;';
      }
      this.db.query(query, pointsAdd, (err) => {

        if (err) {

          return reject(Boom.internal('Internal DB error'));
        }

        return resolve();
      });
    })).then((res) => {

      cb(null, res);

    }).catch((e) => {

      console.error(e);
      cb(e);

    });

  }

  delete(id, cb) {

    const query = 'DELETE FROM cases WHERE id = ?; ' +
        'DELETE FROM cases_info WHERE id_case = ?; ' +
        'DELETE FROM cases_tags WHERE id_case = ?; ' +
        'DELETE FROM points WHERE id_case = ?; ' +
        'DELETE FROM media WHERE id_case = ?; ';
    this.db.query(query, [id, id, id, id, id], (err) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb();
    });
  }

  get(id, cb) {

    const query = 'SELECT * FROM cases WHERE id = ?;' +
        'SELECT * FROM cases_info WHERE id_case = ?;' +
        'SELECT * FROM media WHERE id_case = ?;' +
        'SELECT * FROM points WHERE id_case = ?;' +
        'SELECT * FROM cases_tags LEFT JOIN tags ON (cases_tags.id_tag = tags.id) WHERE id_case = ?;';
    this.db.query(query, [id, id, id, id, id], (err, res) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      if (res[0].length === 0 || res[1].length === 0) {

        return cb(Boom.notFound());
      }

      const response = {
        caseModel: res[0][0],
        caseInfo: res[1],
        media: res[2],
        points: res[3],
        tags: res[4]
      };

      return cb(null, response);
    });
  }

  addTags(id, tags, cb) {

    let query = '';
    const params = [];
    tags.forEach((e) => {
      params.push({ id_case: id, id_tag: e });
      query += 'INSERT INTO cases_tags SET ?;';
    });
    this.db.query(query, params, (err) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb();
    });
  }

}

const create = function create(params) {

  return new Case(params);
};

module.exports = {
  create
};
