/* eslint-disable consistent-return,hapi/hapi-scope-start,no-plusplus,no-param-reassign,newline-per-chained-call,max-len */

const debug = require('debug')('api:models:cases');
const Boom = require('boom');

debug('Loading tag model.');


class Tag {

  constructor({ db, config, helper }) {

    this.db = db;
    this.config = config;
    this.helper = helper;
  }

  list(cb) {
    const query = 'SELECT * FROM tags; ' +
        'SELECT * FROM tag_categories;';
    this.db.query(query, (err, res) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      const categories = res[1];
      const tags = res[0];
      categories.forEach((c) => {

        c.tags = [];
        tags.forEach((t) => {
          if (c.id === t.id_category) {
            c.tags.push(t);
          }
        });
      });


      return cb(null, categories);
    });

  }

  insert({ name, id_category }, cb) {

    const query = 'INSERT INTO tags SET ?;';
    this.db.query(query, [{ name, id_category }], (err, res) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb(null, res.insertId);
    });
  }

  insertCategory(name, cb) {

    const query = 'INSERT INTO tag_categories SET ?;';
    this.db.query(query, [{ name }], (err, res) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb(null, res.insertId);
    });

  }

  delete(id, cb) {

    const query = 'DELETE FROM tags WHERE id = ?';
    this.db.query(query, [id], (err) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb();
    });
  }

  deleteCategory(id, cb) {

    const query = 'DELETE FROM tags WHERE id_category = ?;' +
          'DELETE FROM tag_categories WHERE id = ?;';
    this.db.query(query, [id, id], (err) => {

      if (err) {

        return cb(Boom.internal('Internal DB error'));
      }

      return cb();
    });
  }

}

const create = function create(params) {

  return new Tag(params);
};

module.exports = {
  create
};
