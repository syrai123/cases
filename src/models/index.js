const UserModel = require('./user');
const TagModel = require('./tag');
const CaseModel = require('./case');

module.exports = {
  UserModel,
  TagModel,
  CaseModel
};
