request('/api/v1/tags', null, showEnableCategory)

const category = document.getElementById('category');
const tagsContainer = document.getElementById('tagMenu');
const containerForFullDescription =  document.getElementById('containerForFullDescription');
const tagArray = [];

tagsContainer.innerHTML = '';
addFullDescription();
category.onclick = () => {
    if(!tagsContainer.childNodes.length){
        request('/api/v1/tags', null, showChosePanel);
    }
    else if(getComputedStyle(tagsContainer).display === "none"){
        tagsContainer.style.display = 'block';
    }
    else tagsContainer.style.display = 'none';
};

category.style.cursor = 'pointer';

function showChosePanel(info) {
    info.forEach(cur => {
        tagsContainer.appendChild(document.createElement('hr'));
        const curContainer = document.createElement('div');
        let name = document.createElement('span');
        name.innerHTML = `${cur.name}`;
        curContainer.appendChild(name);

        let menu = document.createElement('ul');
        cur.tags.forEach(curTag => {
            let sub = document.createElement('li');
            let input = document.createElement('input');
            input.setAttribute('data-id', `${curTag.id}`);
            input.setAttribute('type', 'checkbox');
            sub.appendChild(input);

            let span = document.createElement('span');
            span.innerHTML = `${curTag.name}`;
            sub.appendChild(span);
            sub.setAttribute('data-id', `${curTag.id}`);
            menu.appendChild(sub);

            input.addEventListener('change', () => {
                if(tagArray.indexOf(curTag.id) === -1) tagArray.push(curTag.id);
                else tagArray.splice(tagArray.indexOf(curTag.id), 1);
            });

        });
        curContainer.appendChild(menu);
        tagsContainer.appendChild(curContainer);
    })
}

function request(url, param, cb) {
    let add = param ? new URLSearchParams(Object.entries(param)) : '';
    fetch(url + add)
        .then(response => {
            if (response.status !== 200) {
                console.log('Error: ' + response.status);
                return;
            }

            response.json()
                .then(data => {
                    cb(data);
                });
        });
}
function deleteRequest(url, cb) {
    fetch(url, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `${localStorage.getItem('authorization')}`
        },
    })
        .then(response => {
            if (response.status !== 204) {
                console.log('Error: ' + response.status);
                return;
            }

            alert('Кейс видалено');
        });
}
function postRequest(url, param, cb) {

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `${localStorage.getItem('authorization')}`
        },
        body: JSON.stringify(param)
    })
        .then(response => {
            if (response.status !== 200) {
                console.log('Error: ' + response.status);
                return;
            }
            response.json()
                .then(data => {
                    cb(data);
                })

        })
        .catch(e => {
            alert(e);
        })
}
function createCase() {
    const points = [];
    const form = document.getElementById('form');
    const fullDescription = document.getElementById('containerForFullDescription');

    if(!(form.name.value &&
            form.authors.value &&
            form.countries.value &&
            form.language.value &&
            form.date.value &&
            form.shortDescription.value
        ))
    {
        document.getElementById('er').style.color = 'red';
        document.getElementById('er').innerHTML = 'Є пусті поля';
        window.scrollTo(0, 0);
        return;
    }

    let er = false;
    let count = 0;
    Array.from(fullDescription.children, div => {
        console.log(fullDescription);
        let name = div.children[0].value;
        let description = div.children[1].value;

        if (!name && !description) {
            document.getElementById('er').style.color = 'red';
            document.getElementById('er').innerHTML = 'Є пусті поля';
            window.scrollTo(0, 0);
            er = true;
        } else {
            if (name === 'Види взаємодії') {

                //країна дата та автори
                count = count + 3;
            }
            points.push({
                position: ++count,
                type: "point",
                language: "ua",
                name: name,
                description: description
            });
        }
    });
    console.log(er);
    console.log(tagArray);
    if(er)document.getElementById('er').innerHTML = '';
    else {
        const param = {
            caseModel: {
                small_image: '/public/NO_PHOTO.JPG'
            },
            caseInfo: [{
                language: form.language.value,
                name: form.name.value,
                short_description: form.shortDescription.value,
                authors: form.authors.value,
                countries: form.countries.value,
                date: form.date.value
            }],
            tags: tagArray.map(cur => {return {id_tag: cur}}),
            media: [
                {
                    reference: '/public/NO_PHOTO.JPG',
                    type: "image"
                }
            ],
            points: points
        };

        console.log(param)

        postRequest('/api/v1/cases', param, result => alert('Додано'));

    }

}
function addFullDescription() {
    const headers = [
        'Комерційне подання', 'Організація (-ії)', 'Види взаємодії', 'Підтримуючий механізм', 'Резюме', 'Обгрунтування', 'Цілі', 'Відповідальність', 'Cтратегія і вжиті заходи',
        'Моніторинг та оцінювання', 'Заходи для сталого розвитку', 'Вартість',
        'Фінансування', 'Результати', 'Наслідки', 'Зацікавлені сторони бенефіціари',
        'Нагороди / визнання', 'Основні виклики', 'Фактори успіху', 'Мобільність', 'Публікації / статті',
        'Посилання', 'Ключові слова', 'Контактна інформація', 'Модель UIC відповідно до КАБРІОЛЕТ класифікатора',
        'Верифікація моделі UIC', 'Зміна рівня готовності технології (TRL) за результатами проекту',
        'Оцінка успішності проекту'
    ];
    headers.forEach(header => {
        const container = document.createElement('div');
        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('placeholder', 'Заголовок');
        input.style.width = '300px';
        input.value = header;
        container.appendChild(input);

        //container.innerHTML += '<br>';

        const description = document.createElement('textarea');
        description.setAttribute('placeholder', 'Опис');
        description.value = 'Даних немає';
        container.appendChild(description);

        //container.innerHTML += '<br>';

        containerForFullDescription.appendChild(container)
    });


}

function deleteCaseFromDB() {
    const id = Number(document.getElementById('deleteLine').value);
    if(id) deleteRequest(`/api/v1/cases/${id}`, result => alert('Кейс видалено'));
}

function addCategory() {
    const form = document.getElementById('formAddCategory');

    postRequest('/api/v1/tags', {
        id_category: Number(form.id.value),
        name: form.tag.value
    }, () => alert('Додано'))
}

function showEnableCategory(info) {
    const categoryList = document.getElementById('categoryList');

    info.forEach(category => {
        let li = document.createElement('li');
        li.innerHTML = `${category.name} - id: ${category.id}`;
        categoryList.appendChild(li);
    })
}

function authorization() {
    const form = document.getElementById('authorizationForm');
    const result = document.getElementById('resultAuthorizationForm');
    const authorization = document.getElementById('authorization');

    result.style.color = 'red';

    if(!form.login.value && !form.pas.value) result.innerHTML = 'Є пусті поля';

    const req = {
        login: form.login.value,
        password: form.pas.value
    };
    fetch('api/v1/login', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req)
    })
        .then(response => {
            if(response.status === 200){
                localStorage.setItem("authorization", response.headers.get('authorization'));
                result.innerHTML = 'Успіх!';
                setTimeout(() =>  document.getElementById('authorization').style.display = 'none', 1500);
                location.assign('secure/adminka')
            }

            else if(response.status === 401){
                result.innerHTML = 'Невірний логін або пароль';
            }
        })
}

function showWindow() {
    document.getElementById('authorization').style.display = 'block';
}
function backShowWindow() {
    document.getElementById('authorization').style.display = 'none';
}

///////////////////////EDITING///////////////////////////

let mode = 'creating'; // creating/editing      by default - creating. creating/editing

const addCaseFullDescription = (info) => {


    console.log(info)

    info.points.forEach(point => {
        const container = document.createElement('div');
        const input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('placeholder', 'Заголовок');
        input.style.width = '300px';
        input.value = point.name;
        container.appendChild(input);

        //container.innerHTML += '<br>';

        const description = document.createElement('textarea');
        description.setAttribute('placeholder', 'Опис');
        description.value = point.description;
        container.appendChild(description);

        containerForFullDescription.appendChild(container);

    });
    const form = document.getElementById('form');
    form.name.value  = info.caseInfo[0].name;
    form.authors.value  = info.caseInfo[0].authors;
    form.countries.value = info.caseInfo[0].countries;
    form.language.value = info.caseInfo[0].language;
    form.date.value = info.caseInfo[0].date;
    form.shortDescription.value = info.caseInfo[0].short_description;


}

const creatingMode = () => {

    mode = 'creating';
    // clear container
    document.getElementById('containerForFullDescription').innerHTML = '';
    addFullDescription();
};

function requestOnCase(url, cb) {


    fetch(url)
        .then(response => {
            if (response.status !== 200) {
                alert('Такого кейсу намає!');
                creatingMode();
                return;
            }

            response.json()
                .then(data => {
                    cb(data);
                });
        })
        .catch(e => console.log(e));

}
const getCaseById = () => {

    const id = Number(document.getElementById('editLine').value);
    if(!id) {

        alert('Введіть id, а потім тисніть кнопку "Редагування"');
        creatingMode();
        return;
    } requestOnCase('/api/v1/cases/' + id, addCaseFullDescription);
}

const editingMode = () => {

    mode = 'editing';
    // clear container
    document.getElementById('containerForFullDescription').innerHTML = '';
    getCaseById();
};




const createEditCase = () => {


    const points = [];
    const form = document.getElementById('form');
    const fullDescription = document.getElementById('containerForFullDescription');

    if(!(form.name.value &&
            form.authors.value &&
            form.countries.value &&
            form.language.value &&
            form.date.value &&
            form.shortDescription.value
        ))
    {
        document.getElementById('er').style.color = 'red';
        document.getElementById('er').innerHTML = 'Є пусті поля';
        window.scrollTo(0, 0);
        return;
    }

    let er = false;
    let count = 0;
    Array.from(fullDescription.children, div => {
        console.log(fullDescription);
        let name = div.children[0].value;
        let description = div.children[1].value;

        if (!name && !description) {
            document.getElementById('er').style.color = 'red';
            document.getElementById('er').innerHTML = 'Є пусті поля';
            window.scrollTo(0, 0);
            er = true;
        } else {
            if (name === 'Види взаємодії') {

                //країна дата та автори
                count = count + 3;
            }
            points.push({
                position: ++count,
                type: "point",
                language: "ua",
                name: name,
                description: description
            });
        }
    });
    console.log(er);
    console.log(tagArray);
    if(er)document.getElementById('er').innerHTML = '';
    else {
        const param = {
            caseModel: {
                small_image: '/public/NO_PHOTO.JPG'
            },
            caseInfo: [{
                language: form.language.value,
                name: form.name.value,
                short_description: form.shortDescription.value,
                authors: form.authors.value,
                countries: form.countries.value,
                date: form.date.value
            }],
            tags: tagArray.map(cur => {return {id_tag: cur}}),
            media: [
                {
                    reference: '/public/NO_PHOTO.JPG',
                    type: "image"
                }
            ],
            points: points
        };

        postRequest('/api/v1/cases', param, (_result) => {

            if (mode === 'creating') alert('Додано');
            else  {

                alert('Відредаговано');
                const id = Number(document.getElementById('editLine').value);
                if(id) deleteRequest(`/api/v1/cases/${id}`);
            }


        });

    }
};


function showIdListCase(){
    request('../api/v1/cases/count', undefined, size =>
        request('../api/v1/cases?', {position: 0, count: size}, list =>{
            document.getElementById('containerCasesListWithID').style.display = 'block';
            const casesList = document.getElementById('listCases');
            casesList.innerHTML = '';

            list.forEach(cur => {
                const text = document.createElement('p');
                text.innerHTML = `${cur.name}: - id ${cur.id_case}`;
                casesList.appendChild(text);
            });
            let btn =document.createElement('button');
            btn.innerHTML = 'Назад';
            btn.onclick = hideСontainerCasesListWithID;
            casesList.appendChild(btn);
        }));
}
function hideСontainerCasesListWithID() {
    document.getElementById('containerCasesListWithID').style.display = 'none';
}

