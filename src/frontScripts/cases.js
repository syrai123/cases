request('./api/v1/cases/count', undefined, size =>
    request('./api/v1/cases?', {position: 0, count: size}, prepareToShowList));
request('./api/v1/tags', undefined, showAvailableTagName);

const elem = document.getElementById('filters');

elem.onclick = function (event) {
    const target = event.target;

    if(target.tagName === 'SPAN') menuItemHandler(target);
    else if(target.tagName === 'LI') menuItemLiHandler(target);
    else if(target.tagName === 'INPUT' && target.getAttribute('type') === 'checkbox'){

        const curTag = Number(target.getAttribute('data-id'));

        if(checkedTags.indexOf(curTag) === -1) checkedTags.push(curTag);
        else checkedTags.splice(checkedTags.indexOf(curTag), 1);
    }
};

const checkedTags = [];
function prepareToShowList(list) {
    console.log(list);

    const size = list.length;
    const btnContainer = document.getElementById('btnContainer');
    btnContainer.innerHTML = '';

    for(let i = 0, j = 1; i < size; i += 5, j++){
        let btn = document.createElement('button');
        btn.innerHTML = j;
        btn.onclick = () => showListCase(list.slice(i, i+5));
        btnContainer.appendChild(btn);
        if(i === 0) showListCase(list.slice(i, i+5));

        //showListCase
    }

    // show case count

    const h1 = document.getElementById('cases_count');
    h1.innerHTML = 'Головна/Кейси: '+ size;


}

function request(url, param, cb) {
    let add;
    if(param){
        if(param.hasOwnProperty('search')){
            let line = param.search;
            line = line.split('').map((cur, i) => {
                if (cur === ' ') return line[i] = '%20';
                else return cur;
            }).join('');

            fetch(`${url}search=${line}`)
                .then(response => {
                    if (response.status !== 200) {
                        console.log('Error: ' + response.status);
                        return;
                    }

                    response.json()
                        .then(data => {
                            cb(data);
                        });
                })
                .catch(e => console.log(e));

            return;
        }
        else add = new URLSearchParams(Object.entries(param));
    }
    else add = '';

    fetch(url + add)
        .then(response => {
            if (response.status !== 200) {
                console.log('Error: ' + response.status);
                return;
            }

            response.json()
                .then(data => {
                    cb(data);
                });
        })
        .catch(e => console.log(e));

}

function currentCase(id) {
    request(`./api/v1/cases/${id}`,null, showCurrentCase)
}
function showListCase(list) {
    console.log(list);

    window.scrollTo(0, 0);
    const container = document.getElementById('cases');
    container.innerHTML = '';


    list.forEach(cur => {
        const curContainer = document.createElement('div');
        curContainer.className = 'projects-container__project';

        let header = document.createElement('h2');
        header.innerHTML = `${cur.name}`;
        header.className = 'cursor';
        curContainer.appendChild(header);
        header.addEventListener('click', () => currentCase(cur.id_case));

        //  let div = document.createElement('div');
        //  div.className = 'projects-container__project__langNdate-info';
        //  let nestedDiv = document.createElement('div');
        //  let span = document.createElement('span');
        //  span.appendChild(tagList(cur));

        // nestedDiv.appendChild(span);
        //  div.appendChild(nestedDiv);

        // nestedDiv = document.createElement('div');
        // span = document.createElement('span');
        // span.innerHTML = `${cur.date}`;
        // nestedDiv.appendChild(span);
        // div.appendChild(nestedDiv);

        // curContainer.appendChild(div);

        //   div = document.createElement('div');
        //    div.className = 'projects-container__project__image';

        //   let img = document.createElement('img');
        //   img.src = `${cur.small_image}`;
        //    div.appendChild(img);

        //  curContainer.appendChild(div);

        //    div = document.createElement('div');
        //    div.className = 'projects-container__project__teamNcountry-info';
        //    nestedDiv = document.createElement('div');
        //    span = document.createElement('span');
        //    span.innerHTML = `Team lead: ${cur.authors}`;
        //     nestedDiv.appendChild(span);
        //    div.appendChild(nestedDiv);

        //    nestedDiv = document.createElement('div');
        //    span = document.createElement('span');
        //    span.innerHTML = `${cur.countries}`;
        //    nestedDiv.appendChild(span);
        //    div.appendChild(nestedDiv);
        //    curContainer.appendChild(div);

        div = document.createElement('div');
        div.className = 'projects-container__project__description';
        let p = document.createElement('p');
        p.innerHTML = `${cur.short_description}`;
        div.appendChild(p);
        curContainer.appendChild(div);

        p = document.createElement('p');
        p.innerHTML = 'Докладніше';
        p.addEventListener('click', () => currentCase(cur.id_case));
        p.className = 'cursor';
        div.appendChild(p);

        container.appendChild(curContainer);
    })
}
function tagList(cur) {
    try{
        const container = document.createElement('div');

        Array.from(cur.tags, tag => {
            let curTag = document.createElement('span');
            curTag.innerHTML = `#${tag.name} `;
            curTag.className = 'cursor';
            curTag.addEventListener('click', () => {
                request('./api/v1/cases?', {position: 0, tags: convertStringToArray([tag.id])}, prepareToShowList);
            });

            curTag.setAttribute('data-id', `${tag.id}`);
            container.appendChild(curTag);
        });
        return container;
    }
    catch(e) {
        const container = document.createElement('div');

        let curTag = document. createElement('span');
        curTag.innerHTML = ``;
        container.appendChild(curTag);
        return container
    }

}
function showCurrentCase(info) {
    location.href = `/cases/${info.caseModel.id}`;
}

function filterCase() {
    request('./api/v1/cases?', {position: 0, tags: convertStringToArray(checkedTags)}, prepareToShowList);
}
function convertStringToArray(ar) {
    console.log(ar);
    ar = ar.toString();
    return `[${ar}]`
}
function showAvailableTagName(info) {
    const tagsContainer = document.getElementById('filters');

    info.forEach(cur => {
        tagsContainer.appendChild(document.createElement('hr'));
        const curContainer = document.createElement('div');
        curContainer.className = 'filtering__container__menu';
        let name = document.createElement('span');
        name.className = 'filtering__container__menu__title';
        name.innerHTML = `${cur.name}`;
        curContainer.appendChild(name);

        let menu = document.createElement('ul');
        cur.tags.forEach(curTag => {
            let sub = document.createElement('li');
            let input = document.createElement('input');

            input.setAttribute('type', 'checkbox');
            input.className = 'checkbox';
            input.setAttribute('data-id', `${curTag.id}`);
            let span = document.createElement('span');
            span.innerHTML = `${curTag.name}`;
            sub.appendChild(input);
            sub.appendChild(span);
            sub.setAttribute('data-id', `${curTag.id_tag}`);
            menu.appendChild(sub);


        });
        curContainer.appendChild(menu);
        tagsContainer.appendChild(curContainer);
    })
}





function menuItemHandler(target) {
    target.parentNode.classList.toggle('open');

}

function menuItemLiHandler(target) {
    if(!target.firstChild.getAttribute('checked'))
        target.firstChild.setAttribute('checked', 'true');

    else
        target.firstChild.removeAttribute('checked');
}


const search = document.getElementById('search');
search.style.margin = '3px';
search.style.cursor = 'pointer';

search.addEventListener('click', event => {
    const target = event.target;
    const line = document.getElementById('searchLine').value;

    if(target.tagName === 'I') {
        if(!line) return;

        request('/api/v1/cases?', {search: line}, result => prepareToShowList(result))
    }
});


