const Boom = require('boom');
const debug = require('debug')('api:plugins:authjwt');

debug('Loading auth JWT plugin.');

const internals = {
  logTags: ['auth-jwt', 'auth']
};

internals.applyAuth = function applyAuth(server, next) {

  const validate = function validate(decoded, request, callback) {

    const timeNow = Date.now();
    const valid = !!decoded.valid && decoded.exp >= timeNow;

    if (!valid) {
      callback(null, valid, decoded);

      return;
    }
    const workers = {
      db: request.app.db,
      config: internals.config,
      helper: internals.helper
    };

    const um = internals.models.UserModel.create(workers);

    um.getHash(decoded.id, (err, result) => {

      if (err) {
        debug(err);

        callback(Boom.internal('Internal DB error', err));

        return;
      }

      if (!result || result.password !== decoded.hash) {
        callback(Boom.unauthorized('Token unauthorized!', 'JWT'));
      } else {

        const data = Object.assign({}, decoded);
        data.isAdmin = () => decoded.role[0] === 'admin';
        data.isSame = id => decoded.id === Number(id);

                // returned as request.auth.credentials
        callback(null, true, data);
      }

    });
  };

  server.auth.strategy('jwt', 'jwt', false,
    {
      key: internals.config.jwtSecret,
      verifyFunc: validate,
      validateFunc: validate,
      allowUrlToken: false,
      tokenType: 'Token',
      verifyOptions: {
        algorithms: ['HS256']
      }
    }
    );

  server.auth.default('jwt');

  server.log(internals.logTags, 'Initialized jwtAuth.');

  return next();
};

module.exports.register = function register(server, options, next) {

    // TODO: hoek check parameters
  internals.config = options.config;
  internals.models = options.models;

  server.dependency(['hapi-auth-jwt2'], internals.applyAuth);
  next();
};

module.exports.register.attributes = {
  name: 'auth-jwt',
  once: true
};

