const JWT = require('jsonwebtoken');
const debug = require('debug')('api:plugins:authjwtrefresher');

debug('Loading auth JWT refresher plugin.');

const internals = {
  logTags: ['auth-jwtrefresher', 'auth']
};

function refreshJWT(request, reply) {

  const { isAuthenticated, credentials, strategy } = request.auth;

  if (isAuthenticated && credentials && strategy === 'jwt') {

    const now = new Date().getTime();
    const remaining = credentials.exp - now;

    if (remaining <= internals.config.refreshTime) {
      debug('Passed refresh time. Issuing new Token');

      // copy credential settings
      const newCredentials = Object.assign({}, credentials);

      // extend token
      newCredentials.iat = now; // created now
      newCredentials.exp = now + internals.config.lifeTime;

      // sign the session as a JWT
      const token = JWT.sign(newCredentials, internals.config.jwtSecret ); // synchronous

      // return as cookie
      request.response
        .header('Authorization', `Token ${token}`)
        .state('token', token, internals.config.cookie);
    }
  }

  reply.continue();
}

internals.jwtRefresher = function jwtRefresher(server, next) {

  server.ext({
    type: 'onPreResponse',
    method: refreshJWT
  });

  server.log(internals.logTags, 'Initialized jwtAuthRefresher.');
  next();
};

module.exports.register = function register(server, options, next) {

  // TODO: hoek check parameters
  internals.config = options.config;

  server.dependency(['hapi-auth-jwt2', 'auth-jwt'], internals.jwtRefresher);
  next();
};

module.exports.register.attributes = {
  name: 'auth-jwtrefresher',
  once: true
};
