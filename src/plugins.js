/* eslint-disable import/no-extraneous-dependencies */
const config = require('config');

// External Plugins

const Blipp = require('blipp');
const Tv = require('tv');
const InertPlugin = require('inert');
const VisionPlugin = require('vision');
const ErrorPlugin = require('hapi-error');
const SwaggerPlugin = require('hapi-swagger');
const JwtPlugin = require('hapi-auth-jwt2');
const GoodPlugin = require('good');
const MysqlPlugin = require('hapi-plugin-mysql');

// External Libs
const JWT = require('jsonwebtoken');

// Internal Plugins
const AuthJwtPlugin = require('./plugins/auth_jwt');
const AuthJwtRefreshPlugin = require('./plugins/auth_jwt_refresher');

// Database Models
const models = require('./models');

// Helper Functions
const helper = require('./helper');

// Routes
const AuthRoute = require('./routes/auth');
const FrontRoute = require('./routes/front');
const CaseRoute = require('./routes/case');
const TagRoute = require('./routes/tag');

// Handlers
const authHandlers = require('./handlers/auth');
const frontHandlers = require('./handlers/front');
const caseHandlers = require('./handlers/case');
const tagHandlers = require('./handlers/tag');

const libs = {
  JWT
};

module.exports = [
  { register: InertPlugin },
  { register: VisionPlugin },
  { register: ErrorPlugin },
  { register: SwaggerPlugin },
  { register: Blipp },
  { register: Tv, options: config.tv },
  { register: JwtPlugin },
  { register: AuthJwtPlugin, options: { config, models, libs } },
  { register: AuthJwtRefreshPlugin, options: { config, libs } },

  { register: GoodPlugin, options: config.good },

  { register: MysqlPlugin, options: config.db },

  { register: AuthRoute,
    routes: { prefix: config.apiPrefix },
    options: { handlers: authHandlers.create({ config, models, helper, libs }) } },
  { register: FrontRoute,
    options: { handlers: frontHandlers.create({ config, models, helper, libs }) } },
  { register: CaseRoute,
    routes: { prefix: config.apiPrefix },
    options: { handlers: caseHandlers.create({ config, models, helper, libs }) } },
  { register: TagRoute,
    routes: { prefix: config.apiPrefix },
    options: { handlers: tagHandlers.create({ config, models, helper, libs }) } }
];

