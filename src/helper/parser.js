/* eslint-disable no-plusplus */
const parseFromTo = (text, { from, to }) => {

  let newText = '';

  if (!text || !from || !to) {
    return text;
  }
  const fromLength = from.length;
  for (let i = 0; i < text.length; i++) {

    if (text.substr(i, fromLength) === from) {

      newText += to;
    } else {
      newText += text.charAt(i);
    }

  }
  return newText;
};

const parseToArrBy = (text, by) => {

  const newText = [];

  if (!text || !by) {
    return text;
  }
  const fromLength = by.length;
  let temp = '';
  for (let i = 0; i < text.length; i++) {

    if (text.substr(i, fromLength) === by) {

      newText.push(temp);
      temp = '';
    } else {
      temp += text.charAt(i);
    }

  }
  if (newText.length === 0) newText.push(text);
  return newText;
};


module.exports = {
  parseFromTo,
  parseToArrBy
};
