/* eslint-disable no-plusplus */

const correct = (text, symbolsLength) => {

  if (!text || typeof text !== 'string' || text.length < symbolsLength) {

    return text;
  }
  for (let i = symbolsLength; i > 0; i--) {

    if (text.charAt(i) === ' ') {

      return `${text.substr(0, i)}...`;
    }
  }

  return `${text.substr(0, symbolsLength)}...`;
};

const correctArrByParam = (arr, param, symbolsLength) => {

  console.log(arr);
  arr.forEach((e) => {

    e[param] = correct(e[param], symbolsLength);
  });
};

module.exports = {
  correct,
  correctArrByParam
};
