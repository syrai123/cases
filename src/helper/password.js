const Bcrypt = require('bcrypt');

const saltRounds = 12;

const createPassword = function hashPassword(password, cb) {

  Bcrypt.hash(password, saltRounds, cb);
};

const comparePassword = function comparePassword(password, hash, cb) {

  Bcrypt.compare(password, hash, cb);
};

module.exports = {
  createPassword,
  comparePassword
};
//createPassword('HarDPa558ord', (e,r) => console.log(r))