/* eslint-disable no-param-reassign */
/**
 *
 * по возрастанию
 */
const sort = (arr, param) => {

  if (!param || !arr || !Array.isArray(arr) || arr.length < 2) return arr;

  let isSorted = false;

  while (!isSorted) {

    isSorted = true;

    for (let i = 0; i < arr.length - 1; i++) {

      if (arr[i][param] > arr[i + 1][param]) {

        const temp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = temp;
        isSorted = false;
      }
    }
  }

  return arr;


};

module.exports = {
  sort
};

