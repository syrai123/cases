/**
 * This function copies src object's fields into the new object
 * taking in account 'replace' parameter.
 * @param src source object
 * @param replace array that contains objects like [{replace: 'srcFieldName', with: 'newFieldName'}]
 * @returns resulted object
 */
const convert = (src, replace) => {

  const out = {};
  const needReplace = (item, replaceArr = []) => {

    const result = replaceArr.find(elm => elm.replace === item);
    if (result) {
      return result.with;
    }

    return null;
  };

  Object.keys(src).forEach((key) => {

    const newKey = needReplace(key, replace);
    if (newKey) {
      out[newKey] = src[key];
    } else {
      out[key] = src[key];
    }
  });

  return out;
};

module.exports = {
  convert
};
