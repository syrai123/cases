/* eslint-disable no-plusplus */
const select = (text, bySymbol, maxCount) => {

  if (!text || !bySymbol || !maxCount) {
    return text;
  }

  let res = '';
  let i = 0;

  for (; i < maxCount; i++) {

    if (text.charAt(i) === bySymbol) {

      res = `<strong>${text.substr(0, i + 1)}</strong>`;
      break;
    }

  }

  if (res.length === 0) {

    return text;
  }

  res += text.substr(i + 1, text.length - i - 1);

  return res;
};

module.exports = {
  select
};
