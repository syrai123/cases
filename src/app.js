const Hapi = require('hapi');
const Handlebars = require('handlebars');
const config = require('config');
const Plugins = require('./plugins');

function toJSON(conf) {

  return JSON.parse(JSON.stringify(conf));
}

const createServer = function createServer(conf, cb) {

  const server = new Hapi.Server();

  server.connection( toJSON(conf) );

  server.register( Plugins, (err) => {

    // $lab:coverage:off$
    if (err) {
      cb(err);

      return;
    }

    server.views({
      engines: {
        html: Handlebars
      },
      path: `${__dirname}/templates`
    });
    // $lab:coverage:off$

    server.initialize( initerr => cb(initerr, server));
  });
};

// $lab:coverage:off$
if (!module.parent) {
  createServer(config.server, (err, server) => {

    if (err) {
      throw err;
    }

    server.start( (starterr) => {

      if (err) {
        throw starterr;
      }

      process.stdout.write(`Server running at: ${server.info.uri}`);
    });
  });
}
// $lab:coverage:on$

module.exports = createServer;
