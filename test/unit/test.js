// TODO
console.log(JSON.stringify(

  {
    caseModel: {
      small_image: '/public/NO_PHOTO.JPG'
    },
    caseInfo: [
      {
        language: 'all',
        name: 'Забезпечення та оцінювання безпеки інформаційно-управляючих систем та створення spin-off компанії за участю викладачів університетів та студентів. Розробка моделі UIC взаємодії – від студента до інженера',
        short_description: 'Прикладом успішної кооперації університетів України і індустріальних компаній University-Industry Cooperation (UIC) в галузі забезпечення та оцінювання безпеки інформаційно-управляючих систем (ІУС) є створення на базі кафедри комп’ютерних систем та мереж Національного аерокосмічного університету імені М.Є Жуковського науково-технічного центру дослідження і аналізу безпеки інфраструктур (НТЦ ДАБІ), в основу діяльності якого було покладено модель spin off (різновид дочірньої компанії). Директором центру є завідувач кафедрою кафедри комп’ютерних систем та мереж Національного аерокосмічного університету імені М.Є Жуковського «ХАІ» д.т.н., професор Харченко Вячеслав Сергійович. Центр було створено для підтримки діяльності ПАТ НВП «Радій» при ліцензуванні та сертифікації ІУС АЕС в Україні та за кордоном. Унікальність цього проекту полягає у принципах реалізації місії, яка покладається на співробітників центру.\n' +
        '        Основними принципами є:\n' +
        '          -  вивчення та застосування бази міжнародних стандартів в галузі забезпечення та оцінювання безпеки інформаційно-управляючих систем;\n' +
        '-  спадкоємність наукових шкіл різних поколінь;\n' +
        '-  максимально тісний взаємозв’язок теоретичної бази та чіткого розуміння практичних цілей і методів їх застосування.\n' +
        '  Молоді науковці за підтримки своїх наставників мають змогу більш ефективно впроваджувати результати досліджень у світі, який швидко змінюється. Поєднання наукового і бізнес-орієнтованого досвіду в галузі технічного консалтингу дозволило отримати унікальні для українського та світового ринку результати. Прикладом практичного результату діяльності центру є підтримка ліцензування управляючої системи безпеки, яку було розроблено для АЕС Козлодуй (Болгарія).\n' +
        '  Основні підходи до UIC, які покладено в основу отриманих досягнень:\n' +
        '  -  побудова довготривалих бізнес-відношень з власниками бізнеса, та їх загальне розуміння необхідності розвитку такого співробітництва;\n' +
        '-  виконання невеликих успішних проектів, які передують великому проекту;\n' +
        '-  значний досвід та експертні знання у обраній сфері діяльності (у даному випадку – надійності та безпеки ІТ);\n' +
        '-  наробка навичок, які потенційно можуть бути затребувані індустрією;\n' +
        '-  нарощування досвіду міжнародної діяльності та знання англійської мови.\n' +
        '  Як практики в UIC співробітники центру розвивали такі напрями діяльності:\n' +
        '  -  розробка концепції і запуск Українського агентства UIC;\n' +
        '-  здійснення постійного діалогу між представниками індустрії і університетів;\n' +
        '-  регулярне проведення в Україні семінарів BASiC (Workshop on Business Analysis and Project Management for Innovative in Critical Domains);\n' +
        '-  регулярна участь в міжнародних конференціях University Industry Innovation Network, інших форумах, які присвячені UIC;\n' +
        '-  розвиток та практичне втілення положень концепції UIC.\n' +
        '  Наступним кроком розвитку НТЦ ДАБІ в рамках концепції UIC було створення, за участю викладачів Полтавського національного технічного університету імені Юрія Кондратюка, додаткової площадки Poltava_Verification&Validation (V&V) з метою незалежної верифікації та валідації ІУС, які розробляються та виготовляються ПАТ НВП «Радій». Результатом створення додаткової spin off компанії стала розробка та впровадження технології незалежної верифікації та валідації повного життєвого циклу ІУС, які розробляються на базі ПЛІС (програмовані логічні інтегральні схеми) за технологіями FPGA (Field-Programmable Gate Array) та CPLD (Complex Programmed Logic Device). Розроблена та впроваджена технологія реалізує наступні основні види діяльності:\n' +
        '  -  технічні обзори документації;\n' +
        '-  етапи верифікації електронних проектів ПЛІС (статичний аналіз коду, код ревью, функціональне тестування, часове та логічне моделювання тощо);\n' +
        '-  тестування діагностичних функцій з внесенням програмно-апаратних дефектів (дана технологія має ознаки світової новизни з точки зору реалізації засіву дефектів в модулі ІУС, включаючи апаратну компоненту та програмний код);\n' +
        '-  тестування інтегрованих програмно-апаратних засобів.\n' +
        '  Розроблені технології дозволило spin off компаніям прийняти безпосередню участь у міжнародних проектів:\n' +
        '  -  проведенні сертифікації нової цифрової інформаційно-управляючої платформи безпеки RadICS на відповідність вимогам стандарта МЕК 61508;\n' +
        '-  проведенні сертифікації цифрової інформаційно-управляючої платформи безпеки RadICS на відповідність вимогам регулюючого органу США (Nuclear Regulatory Commision, NRC).\n' +
        'Участь викладачів університеті в зазначених проектах дозволило включити до навчального процесу нові навчальні дисципліни та оновити зміст інших з метою наближення змісту навчання реальним потребам виробництва високотехнологічних систем. Щорічно близько 20 студентів університетів мають змогу проходити виробничу та переддипломну практику. За тематикою робіт, що виконуються щорічно дипломні проекти, а випускники поповнюють кадровий склад як ПАТ НВП «Радій» так і її spin – off компаній.',
        authors: 'Харченко В.С., д.т.н., професор завідувач кафедрою «ХАІ», директор НТЦ ДАБІ,\n' +
'Скляр В.В., д.т.н., професор кафедри «ХАІ», з 2007 до 2011 рр – начальник відділу НТЦ, з 2011 по 2016 рр. – директор технічний НВП Радій;\n' +
'Одарущенко О.М., к.т.н., доцент, менеджер Poltava_Verification&Validation (V&V); Одарущенко О.Б., к.т.н., доцент ПолтНТУ імені Юрія Кондратюка.",\n' +
'"countries": "Україна',
        date: '03.10.2016'
      }
    ],
    points: [
      {
        position: 1,
        type: 'point',
        language: 'all',
        name: 'Комерційна пропозиція',
        description: "Зменшити розрив між змістом освіти для спеціальностей «комп'ютерні науки», «комп'ютерна інженерія», «програмна інженерія», «прикладна математика» в системі вищої освіти і реальних потреб для професіоналів в області розробки інформаційно-управляючих систем та забезпеченням і оцінюванням їх безпеки на основі створення spin off компаній із залученням індустріальних партнерів та представників освіти (викладачі, студенти). Модернізація освітніх програм та навчальних дисциплін на відповідність реальним потребам індустріальних партнерів."
      }, {
        position: 2,
        type: 'point',
        language: 'all',
        name: ' Організація ',
        description: ' ПАТ НВП «Радій»:\n' +
    '    - дослідницька організація;\n' +
    '- глобальна компанія;\n' +
    '- розробка, виготовлення, обслуговування інформаційно-управляючих систем для АЕС України та міжнародних партнерів (Болгарія, Канада, США, Бразилія, Аргентина);\n' +
    '- системна інтеграція.\n' +
    '  Науково-технічний центр дослідження і аналізу безпеки інфраструктур (НТЦ ДАБІ):\n' +
    '-    spin off компанія;\n' +
    '-    дослідницька організація;\n' +
    '-    вивчення та застосування бази міжнародних стандартів в галузі забезпечення та оцінювання безпеки інформаційно-управляючих систем;\n' +
    '-    спадкоємність наукових шкіл різних поколінь;\n' +
    '-    максимально тісний взаємозв’язок теоретичної бази та чіткого розуміння практичних цілей і методів їх застосування.\n' +
    '-\tPoltava_Verification&Validation (V&V):\n' +
    '-    spin off компанія;\n' +
    '-    дослідницька організація;\n' +
    '-    незалежна верифікація та валідація ІУС, які розробляються та виготовляються ПАТ НВП «Радій».\n' +
    'Національний аерокосмічний університет імені М.Є. Жуковського «ХАІ»:\n' +
    '- дослідницька організація;\n' +
    '- освітня організація.\n' +
    '  Полтавський національний технічний університет імені Юрія Кондратюка:\n' +
    '  - дослідницька організація;\n' +
    '- освітня організація. '
      }, {
        position: 3,
        type: 'point',
        language: 'all',
        name: ' Характер взаємодії',
        description: ' Виконання спільних проектів за участю викладачів, студентів та співробітників підприємств, розробка навчальних програм і планів, змісту нових навчальних дисциплін.'
      }, {
        position: 4,
        type: 'point',
        language: 'all',
        name: ' Механізм, що підтримує',
        description: ' Проектна та операційна діяльність'
      }, {
        position: 5,
        type: 'point',
        language: 'all',
        name: ' ПРОФИЛЬ КЕЙСА',
        description: ' Резюме: Дослідження було присвячено інтеграції зусиль університетів і індустріальної компанії для поліпшення інтеграції співробітників в ході виконання реальних проектів, підвищення досвіду викладачів у виконанні високотехнологічних проектів та на підставі цього поліпшення рівня викладання окремих навчальних дисциплін з програмної, комп’ютерної інженерії та прикладної математики.\n' +
  '  Дослідження було викликано тим, що ПАТ НВП Радій потребувало зміцнення кадрового потенціалу для виконання нових міжнародних проектів та результатом дослідження ставилося завдання створення spin-off компаній, співробітники яких будуть залучені до виконання міжнародних проектів.\n' +
  '    Завдання залучення фахівців з ПАТ НВП Радій для розробки навчальних програм та проведення занять (курсів) було необхідним для підвищення практичної складової навчального процесу, вдосконалення матеріально-технічних ресурсів університетів, проходження студентами виробничої практики.\n' +
  '    Обґрунтування: Україна увійшла до топ-5 найбільших виробників атомної енергії та топ-10 у світі. Тому важливими завданням є забезпечення безпечної експлуатації інформаційно-управляючих систем, які є ключовим ланцюгом безпеки функціонування АЕС. Забезпечення та оцінювання безпеки АЕС здійснюється в трикутнику «організація, яка експлуатує станцію» (несе відповідальність за безпечну експлуатацію) – постачальник (несе відповідальність за розробку та виробництво обладнання) – регулюючий орган (державна організація, яка відповідає за державне регулювання, незалежну оцінку та видачу дозволу щодо експлуатації інформаційно-управляючих систем). Після одержання Україною незалежності необхідно було створити структуру, яка мала б виконати технічну підтримку державного регулювання в галузі безпеки АЕС. Для цього було створено харківський філіал Державного науково-технічного Центру з ядерної та інформаційної безпеки. Діяльність Центру включала: вдосконалення нормативної бази та приведення її у відповідність з міжнародними стандартами; участь у міжнародних проектах тощо. Ці завдання вирішувались і вирішуються у кооперації з співробітниками університетів. Початок століття ознаменувався впровадженням нових технологій у розробку та виготовлення інформаційно-управляючих систем (зокрема ПАТ НВП «Радій» почало розробляти ІУС із використанням технологій FPGA та CPLD). Використання нових інформаційних технологій визначило напрямки в сфері нормативного регулювання ІУС АЕС, розширення ролі програмного забезпечення, яке розробляється та постачається, розвиток міжнародної нормативної бази, нових технологій та принципів розробки ІУС АЕС. Перелік нових завдань, які постали перед державними органами ядерного регулювання, виробниками викликало необхідність залучення додаткових інтелектуальних ресурсів. Тому логічним продовженням взаємодії між державними органами та виробниками стало залучення викладацького складу вищих навчальних закладів і у відповідності до цього на базі кафедри комп’ютерних систем та мереж ХАІ було створено науково-технічний центр дослідження і аналізу безпеки інфраструктур. Подальша робота над вирішенням проектних завдань привела до необхідності розширення мережі залучених вищих навчальних закладів та їх співробітників. Результатом цього розширення стало створення spin off компанії Poltava_Verification&Validation (V&V), основним завданням якої стало розробка та впровадження технології незалежної верифікації та валідації повного життєвого циклу ІУС, які розробляються на базі ПЛІС за технологіями FPGA та CPLD. Безпосереднє кадрове комплектування цієї компанії було здійснено із співробітників вищих навчальних закладів та студентів старших років навчання.\n' +
  '    Цілі: 1. Розробити та впровадити низку робочих процедур із виконання завдань верифікації та валідації в ході роботи над міжнародними проектами. Розробка процедур базується на досконалому вивченні та досліджені державних та міжнародних стандартів в галузі забезпечення та оцінювання безпеки інформаційно-управляючих систем.\n' +
  '  2. Розробити та впровадити технології верифікації та валідації електронних проектів ПЛІС (статичного аналізу коду, обзору код, функціонального тестування, часового та логічне моделювання тощо).\n' +
  '  3. Розробити та впровадити технологію тестування діагностичних функцій з внесенням програмно-апаратних дефектів.\n' +
  '  4. Розробити та впровадити технологію тестування інтегрованих програмно-апаратних засобів ІУС.\n' +
  '  5. Залучити викладацький склад вищих навчальних закладів до виконання високотехнологічних проектів.\n' +
  '  6. Залучити студентів до виконання міжнародних проектів, створити базу практик (ПАТ НВП «Радій», НТЦ ДАБІ, Poltava_Verification&Validation (V&V)).\n' +
  '  7. Модернізувати навчальні плани та низку навчальних дисциплін, включивши в них нові науково-технологічні наробки.\n' +
  '    Відповідаль-ність: Харченко В.С., д.т.н., професор, завідувач кафедрою кафедри комп’ютерних систем та мереж Національного аерокосмічного університету імені М.Є Жуковського «ХАІ»,\n' +
  '  Одарущенко О.Б., к.т.н., доцент кафедри прикладної математики, інформатики і математичного моделювання Полтавського національного технічного університету імені Юрія Кондратюка,\n' +
  '    Одарущенко О.М., к.т.н., доцент, менеджер Poltava_Verification&Validation (V&V)'
      }, {
        position: 6,
        type: 'point',
        language: 'all',
        name: ' РЕАЛІЗАЦИЯ та ФІНАНСУВАННЯ',
        description: ' Стратегія та дії, що зроблені: Кейс «Програма співпраці між Національним аерокосмічним університетом імені М.Є. Жуковського «ХАІ», Полтавським національним технічним університетом імені Юрія Кондратюка, ПАТ НВП «Радій»» розроблений на основі стратегії Win – Win, яка дозволяє отримати очікуваний результат всіх сторін. Для досягнення цілей залучені викладачі обох університетів та студенти.\n' +
  '  Заходи для сталого розвитку: Для забезпечення сталого розвитку співпраці в перспективі між Національним аерокосмічним університетом імені М.Є Жуковського «ХАІ», ПолтНТУ імені Юрія Кондратюка ПАТ НВП «Радій» на регулярній основі проводяться семінари та тренінги з технологічних питань. Проводиться робота з розробки перспективних навчальних дисциплін в галузі тестування та Quality Assurance для систем критичного застосування.\n' +
  '    Моніторинг та оцінювання: Для моніторингу та оцінки успішності даного кейсу використовувалися наступні критерії:\n' +
  '    актуальність співпраці, результативність, ефективність, довготривалий ефект та стійкість об’єктів співпраці до зовнішніх впливів. В ролі експертів виступали викладачі Національного аерокосмічного університету імені М.Є Жуковського «ХАІ», ПолтНТУ імені Юрія Кондратюка та співробітники ПАТ НВП «Радій». Процес багатокритерійного прийняття рішень про успішність співпраці дозволив об’єктивно оцінити результати кооперації.\n' +
  '    Кошторис: Основні витрати здійснюються у зв’язку:\n' +
  '    - часовими витратами для проведення лекційних та практичних занять фахівцями ПАТ НВП « Радій»;\n' +
  '  - грошовими витратами на дообладнання лабораторій для забезпечення занять за новими навчальними дисциплінами;\n' +
  '  - проходженням студентами виробничої практики безпосередньо на майданчику ПАТ НВП «Радій» м. Кропивницький (транспортні витрати, харчування тощо).\n' +
  '  Фінансування: Види фінансування:\n' +
  '    - аренда офісних приміщень для роботи spin-off компаній;\n' +
  '  - закупівля необхідного обладнання (комп’ютерної техніки, вимірювальної техніки) для виконання проектних завдань;\n' +
  '  - оплата праці співробітників spin-off компаній.\n' +
  '    У відсотковому співвідношенні обсяги фінансування складають 25% від необхідних обсягів на закупівлю нового обладнання. '
      }, {
        position: 7,
        type: 'point',
        language: 'all',
        name: ' РЕЗУЛЬТАТИ ТА ВПЛИВ',
        description: ' Результати: В результаті.\n' +
  '  1. Розроблено та впроваджено в процеси розробки, верифікації та валідації ІУС, які розробляє ПАТ НВП «Радій» робочі процедури:\n' +
  '    - процедура обзору документів дизайну;\n' +
  '  - процедура статичного аналізу коду та його обзору;\n' +
  '  - процедура функціонального тестування електронних проектів ПЛІС, розроблених мовою VHDL;\n' +
  '  - процедура часового та логічного моделювання;\n' +
  '  - процедура тестування діагностичних функцій з внесенням програмно-апаратних дефектів;\n' +
  '  - процедура тестування інтегрованих програмно-апаратних засобів ІУС.\n' +
  '  2. До роботи в створених spin-off компаніях залучено 12 викладачів ВНЗ партнерів.\n' +
  '  3. Майже 100 студентів пройшли виробничу практику та розробили і захистили дипломні проекти за питаннями, які досліджувались.\n' +
  '  4. 20 студентів працевлаштовані в ПАТ НВП « Радій» та spin-off компаніях на протязі навчання в університетах та після закінчення навчання.\n' +
  '  6. Модернізовано навчальні плани та навчальні дисципліни,\n' +
  '    Наслідки: За час існування даної програми в ПАТ НВП «Радій» та створених spin-off компаніях працює 100%, які навчалися в університетах-партнерах. П’ять співробітників ПАТ НВП «Радій» за сумісництвом викладають лекційні та практичні курси на кафедрах університетів-партнерів. Приклад успішної кооперації впливає на залучення нових співробітників в університетів до участі в міжнародних проектах та бажання абітурієнтів вступити до відповідного ВНЗ.\n' +
  '    Зацікавлені особи та одержувачі вигід: За рахунок співпраці компанія ПАТ НВП «Радій» отримує підготовлених та перспективних спеціалістів з числа студентів, які були залучені до участі в програмі.\n' +
  '    Це дає змогу зменшити час та ресурси на підготовку потенційних співробітників для виконання задач компанії.\n' +
  '    За рахунок співпраці університети-партнери програми отримують фахівців для підготовки та проведення перспективних курсів, а також висококваліфікованих студентів-спеціалістів, що підвищують загальний рівень якості освіти.\n' +
  '    Нагороди / визнання: Співробітники НТЦ постійно отримують сертифікати про підвищення кваліфікації за напрямами задач, що виконуються. Студенти, які працюють під керівництвом співробітників ЕТЦ є переможцями олімпіад з питань діагностування цифрових систем., отримуюють нагороди за кращі доповіді на конференціях ICONE (2014-2017рр.) '
      }, {
        position: 8,
        type: 'point',
        language: 'all',
        name: 'ОДЕРЖАНІ УРОКИ',
        description: 'Основні виклики: На даний час обмежень в подальшому розвитку співпраці немає. Існує проблема безперервного оновлення та зміцнення кадрового складу у зв’язку з переходом співробітників на роботу до інших IT-компаній після отримання досвіду виконання V&V та інших завдань.\n' +
  '  Фактори успіху: Одним з ключових факторів успішної співпраці є зацікавленість трьох сторін (ПАТ НВП «Радій», Національний аерокосмічний університет імені М.Є. Жуковського «ХАІ», Полтавський національний технічний університет імені Юрія Кондратюка, що підтверджується неперервною спільною підтримкою сторін (фінансування, закупівля необхідного обладнання тощо).\n' +
  '  Поширюваність: Відповідний кейс є наочним прикладом успішної співпраці між ВНЗ України та високотехнологічною індустріальною компанією і є прикладом для інших університетів та компаній, а також для Департаменту Освіти і Науки України з метою підвищення якості освіти студентів в сфері інформаційних технологій. '
      }, {
        position: 9,
        type: 'point',
        language: 'all',
        name: ' ДОДАТКОВА ІНФОРМАЦІЯ',
        description: ' Публікації / статті: 1. Харченко В.С. АЭС: 25 лет исследований и практических результатов / Харченко В.С., Скляр В.В. // Карт-Бланш. № 1-2.-2016.- 42-46.\n' +
  '  2. Одарущенко О.Н. Модель и инструментальная поддержка анализа сигналов при оценке функциональной безопасности FPGA-модулей / Одарущенко О.Н., Ивасюк А.О., Фадеева Е,К., Барвинко А.П // Системи обробки інформації. Науково-технічний журнал.Вип. 4(111).- Харків: Харківський університет Повітряних Сил імені Івана Кожедуба, 2013-с.20-22.\n' +
  '  3. Одарущенко О.Н. Обеспечение тестового покрытия для электронных проектов FPGA при оценивании функциональной безопасности по критериям SIL3/ Одарущенко О.Н., Скляр В.В., Резуненко А.А., Гудзь А.С., Щербаченко С.С., Сенаторов А.А, Вовк Е.Д.// Системи обробки інформації. Науково-технічний журнал.Вип. 5(112).- Харків: Харківський університет Повітряних Сил імені Івана Кожедуба, 2013-с.62-65.\n' +
  '  4. V.Kharchenko. Fault-Injection Testing: FIT-Ability, Optimal Procedure and Tool for FPGA-Based Systems SIL Certification / V.Kharchenko, O. Odarushchenko, V.Sklyar,A. Ivasuyk // Proceedings of East-West Design&Test Symposium (EWDTS"2013).- p.188-192.\n' +
  '  5. V.Kharchenko. Assessment of the Reactor Trip System Dependability Two Markov Chains - based Cases/ V.Kharchenko, O. Odarushchenko, D. Butenko, V. Butenko // Proc. of the 10th Int. Conf. on Digital Technologies, Slovak Republic, Zilina, July, 2014. – IEEE Explore. – P.103-109. 6. O. Odarushchenko Markov’s Model and Tool-Based Assessment of Safety-Critical I&C Systems: Gaps of the IEC 61508/ V. Butenkoa, V. Kharchenko, O. Odarushchenkj, P. Popov, V. Sklyar // Proc. 12-th Int. conf. on probabilistic safety assessment and modeling, USA, Hawaii, Honolulu, June 2014 – Access: http://psam12.org/proceedings/paper/ paper_455_1.pdf.\n' +
  '  7. V. Kharchenko. Fault insertion testing of FPGA-based NPP I&C systems: SIL certification issues/ V. Kharchenko, O.Odarushchenko, V. Sklyar, A. Ivasyuk //Proceedings of the 2014 22nd International Conference on Nuclear Engineering ICONE22 July 7-11, 2014, Prague, Czech Republic, p. 5, 2014, pass: http://www.asmeconferences.org/ ICONE22/pdfs/FinalProgram.pdf.\n' +
  '  8. V.Kharchenko. Multi-Fault Injection Testing: Cases for FPGA-Based NPP I&C Systems/ V.Kharchenko, O. Odarushchenko, V.Sklyar // Proceedings of ICONE-23 23rd International Conference on Nuclear Engineering May 17-21, 2015, Chiba, Japan.\n' +
  '  9. V.Kharchenko.. Toward automated FMEDA for complex electronic products / O. V.Kharchenko, Odarushchenko, E. Babeshko, V.Sklyar // Proceedings of the International Conference on Information and Digital Technologies 2015, ISBN 978-1-4673-7185-8 IEEE Catalog Number CFP15CDT-USB.\n' +
  '  Посилання: Website ПАТ НВП «Радій» - www.radiy.com.\n' +
  '    Website НТЦ – www.csis.org.ua\n' +
  '  Website Національного аерокосмічного університету імені М.Є. Жуковського «ХАІ» - www.csn.khai.edu.\n' +
  '    Website Полтавського національного технічного університету імені Юрія Кондратюка – www.pntu.edu.ua.\n' +
  '    Ключові слова: Інформаційно-управляючі системи, функціональна безпека, верифікація і валідація, тестування.\n' +
  '    Контактна інформація: Poltava_Verification&Validation (V&V),\n' +
  '    Post: Ognivskaya str., 8, Poltava, Ukraine 36023;\n' +
  '  tel.: +38-050-590-14-31;odarushchenko@gmail.com. '
      }, {
        position: 10,
        type: 'point',
        language: 'all',
        name: ' ПОДАЛЬШІ РЕЗУЛЬТАТИ ЗА ПРОЕКТОМ',
        description: ' Модель UIC у відповідності до КАБРІОЛЕТ-класифікатора: Модель C підтверджується створенням спін-офф компанії як частини НВП Радій, яка виконує дослідницькі, верифікаційні, ліцезійні та тренінгові функції у газуі функціональної та кібербезпеки ІКС АЕС.\n' +
  '  Верифікація моделі UIC: Самостійне виконання досліджень, публікація результатів, проведення робіт, пов’язаних з ліцензуванням плаиформи RadICS на відповідність вимогам стандарту IEC61508 (компанія exida), вимогам NRC USA до ІКС АЕС розроблення нових процедур тестування в рамках життєвого циклу ІКС, розроблення нових навчальних курсів, тренінгів в цілому підтверджують підтверджують доцільність та обґрунтованість обраної моделі кооперації С.\n' +
  '    Зміна рівня готовності технології(TRL) за результатами проекту: На момент 2016 ріку оцінка готовності кейсу – TRL 9 (actual system proven in operational environment (competitive manufacturing in the case of key enabling technologies; or in space).\n' +
  '  Оцінка успішності проекту: Успішність проекту – 10 балів. Відповідна оцінка пов’язана з довгостроковою успішною кооперацією, яка підтверджується досягненням цілей Національним аерокосмічним університетом імені М.Є. Жуковського «ХАІ», Полтавським національним технічного університетом імені Юрія Кондратюка та ПАТ НВП «Радій».'
      }
    ]
  }


));

