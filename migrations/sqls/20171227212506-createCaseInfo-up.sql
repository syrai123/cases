CREATE TABLE `cases_info` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `id_case`           INT(11) NOT NULL,
  `language`          VARCHAR(8) NOT NULL DEFAULT "all",
  `name`              VARCHAR(1024) NOT NULL,
  `short_description` TEXT,
  `date`              VARCHAR (1024),
  `authors`           VARCHAR (1024),
  `countries`         VARCHAR (1024),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`id` ASC))
ENGINE = InnoDB;