CREATE TABLE `tags` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `id_category`   INT(11) NOT NULL,
  `name`          VARCHAR(64),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`id` ASC))
ENGINE = InnoDB;