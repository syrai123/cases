CREATE TABLE `cases` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `small_image` VARCHAR(255),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`id` ASC))
ENGINE = InnoDB;