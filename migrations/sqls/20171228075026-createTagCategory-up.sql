CREATE TABLE `tag_categories` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `name`          VARCHAR(64),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`id` ASC))
ENGINE = InnoDB;