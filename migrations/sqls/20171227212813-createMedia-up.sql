CREATE TABLE `media` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `id_case`     INT(11) NOT NULL,
  `language`    VARCHAR(8) NOT NULL DEFAULT "all",
  `type`        VARCHAR(64) NOT NULL DEFAULT "image",
  `reference` VARCHAR (1024),
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`id` ASC))
ENGINE = InnoDB;