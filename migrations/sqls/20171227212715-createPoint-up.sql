CREATE TABLE `points` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `id_case`     INT(11),
  `position`    INT(11),
  `language`    VARCHAR(8) default 'all',
  `name`        VARCHAR(256),
  `description` TEXT,
  `type`        VARCHAR(16) NOT NULL DEFAULT "point",
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`id` ASC))
ENGINE = InnoDB;